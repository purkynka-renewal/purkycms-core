const EndpointRouter = require('./EndpointRouter.js');

module.exports = function getAccountRouter() {
  const router = new EndpointRouter();

  //
  // Login
  //
  // Requires `username` and `password`
  router.post("/login", (_,res)=>{
    const params = res.getRequiredParams({
      username: "string",
      password: "string"
    });

    const result = this.login(params.username, params.password);
    if(!result) return res.sendAuthFailure(true);

    return res.sendSuccess({
      token: this.getToken(result).tokenString
    });
  });

  //
  // Relog
  //
  // Requires token to relog
  router.all("/relog", (req,res)=>{
    const data = req.getClientData();
    req.getToken().expire();

    return res.sendSuccess({
      token: this.getToken(data).tokenString
    });
  });

  //
  // Logout
  //
  // Requires token to logout
  router.all("/logout", (req,res)=>{
    req.getToken().expire();
    return res.sendSuccess();
  });

  //
  // Create account
  //
  // Requires `token` in `query` and new creditials in `body`
  router.post("/create", (req,res)=>{
    const params = res.getRequiredParams({
      user: "string",
      password: "string",
      params: "object"
    });
    let result;
    try{
      result = this.createAccount(
        params.user,
        params.password,
        params.params,
        req.getClientId()
      );
    }catch(e){
      return res.sendException(e);
    }
    return res.sendSuccess(result);
  });

  return router;
}
