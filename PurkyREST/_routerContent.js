const {Router} = require('express');
const EndpointRouter = require('./EndpointRouter.js');
const ResourceRouter = require('./ResourceRouter.js');
const e = require("./REST_errors.js");

// TODO: Respond with method not supported instead of 404 for unsupported methods

module.exports = function getContentRouter() {
  const router = new EndpointRouter();

  //
  // Register special paths
  //
  //// Index of content types
  router.get("/_index", (_, res)=>{
    return res.sendSuccess(Object.values(this.types));
  });


  //
  // Register path for each contenttype
  //
  for(const typeName in this.types){
    const type = this.types[typeName];
    const typeRoots = ([
      type.root,
      type.name,
      type.namePlural,
      type.label,
      type.labelPlural,
      ...(type.aliases || [])
    ]
      .map(this.slugify)
      .filter(a=>(a && a != ""))
      .sort((a,b)=>(b.length - a.length))
    );
    const typeRouter = new ResourceRouter();
    router.use(new RegExp(`\/(?:${typeRoots.join("|")})`), typeRouter);

    function getTypeInfo() { return type; }
    //   return {
    //     name: type.name,
    //     namePlural: type.namePlural,
    //     label: type.label,
    //     labelPlural: type.labelPlural,
    //     fields: type.fields,
    //     aliases: type.aliases,
    //     tableStatus: type.table.status
    //   };
    // }

    function wrapData(data={}) {
      return {
        fields: data,
        type  : getTypeInfo()
      }
    }

    //
    // Special paths
    //// Info about the content type
    typeRouter.get("/_contenttype", (_, res)=>{
      return res.sendSuccess(getTypeInfo());
    });

    //// Index of the content
    typeRouter.get("/_index", (_,res)=>{
      return res.sendSuccess([
        "_contenttype",
        "/"
      ]);
    });

    //
    // GET (listing)
    //
    // No requirements
    typeRouter.get("/", (req, res)=>{
      const page = Number(req.query.page) || 0;
      const limit = Number(req.query.limit) || this.options.pageLength;
      const from = page * limit;
      let data;

      if("q" in req.query && req.query.q) {
        data = this.getDataByFTS(typeName, req.query.q, from, limit,
            req.query.orderby);
      } else {
        data = this.getData(typeName, from, limit, req.query.orderby);
      }

      return res.sendSuccess({
        type: getTypeInfo(),
        items: data.map(wrapData)
      });
    });


    //
    // POST - create data
    //
    // Requires `token` in `query` and `body`(data)
    typeRouter.post("/", (req, res)=>{
      const requiredFields = {};
      requiredFields[this.SLUG_FROM] = "string";
      const params = res.getRequiredParams(requiredFields, "body");

      this.insertData(typeName, req.getClientId(), params);

      return res.sendSuccess({
        slug: this.slugify(req.body[this.SLUG_FROM])
      }, 201);
    });


    // // // // //
    // CATEGORIES
    // // // // //
    const categoryRoots = [
      "category",
      "categories",
      ...this.LANG.category
    ];
    const categoryRouter = new Router();
    typeRouter.use(new RegExp(`\/(?:${categoryRoots.join("|")})`), categoryRouter);

    //
    // GET category (listing)
    //
    // No requirements
    categoryRouter.get("/", (_, res)=>{
      return res.sendSuccess(this.getCategories(typeName));
    });

    //
    // GET category/<category> (listing)
    //
    // No requirements
    categoryRouter.get("/:slug", (req, res, next)=>{
      const data = this.getDataByCategory(
        typeName,
        req.params.slug
      ).map(wrapData);
      if(data.length > 0) return res.sendSuccess(data);
      return next(new e.e404());
    });


    // // // // //
    // CONTENT
    // // // // //

    //
    // GET <slug>
    //
    // No requirements
    typeRouter.get("/:slug", (req, res, next)=>{
      const data = this.getDataBySlug(typeName, req.params.slug);
      if(data)  return res.sendSuccess(wrapData(data));
      return next(new e.e404());
    });

    //
    // PUT <slug> - update data
    //
    // Requires `token` in `query` and `body` (data)
    typeRouter.put("/:slug", (req, res)=>{
      this.updateData(typeName, req.params.slug, req.getClientId(), req.body);

      return res.sendSuccess({
        slug: this.slugify(req.body[this.SLUG_FROM])
      }, 200);
    });
  }

  return router;
}
