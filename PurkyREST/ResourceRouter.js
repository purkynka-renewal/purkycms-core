const {Router} = require("express");

module.exports = class ResourceRouter extends Router{
  constructor(...args){
    super(...args);

    //
    // Register special paths
    //
    //// Ping
    this.get("/_ping", (_,res)=>{
      return res.send("pong!");
    });
  }
}
