const {Router} = require('express');
const REST = require("./REST.js");
const {HandledError} = require("./REST_errors.js");
const getContentRouter = require("./_routerContent.js");
const getAccountRouter = require("./_routerAccount.js");

const APP_NAME = "Purky REST API";

module.exports = function () {
  const router = new Router();

  //
  // Register special middleware
  //
  router.use(purkyMiddleware = (req, res, next)=>{
    req.isVirtual = req._parsedUrl.protocol == null;

    // Add REST API methods to response object
    Object.assign(res, REST);

    // Set headers
    res.set({
      'Content-Type': 'application/json',
      'x-powered-by': APP_NAME,
    });

    // If token is present, check it (only if account system is enabled)
    if(!this.options.readOnly &&
        ("token" in req.query || "token" in req.cookies)){
      req._token = this.restTokenCheck(res);
    }
    // Register security functions
    Object.assign(req, {
      getToken() {
        const res = this.res;
        // disable cache for backend-calls
        res.set({
          "Cache-Control": "private, must-revalidate, proxy-revalidate, max-age=0"
        });
        if(this._token) return this._token;
        res.sendAuthFailure();
        throw new HandledError();
      },
      getClientId() {
        return this.getToken().clientId;
      },
      getClientData() {
        return this.getToken().data;
      }
    })

    return next();
  });

  /**
   * restTokenCheck - Checks login token and returns it or sends error as response
   *
   * @param  {type} res   description
   * @param  {type} query description
   * @return {type}       description
   */
  this.restTokenCheck = function restTokenCheck(res) {
    const params = res.getRequiredParams({
      token: "string",
    }, ["query", "cookie"]);
    const token = this.checkToken(params.token);
    if(!token) {
      res.sendAuthFailure();
      throw new HandledError();
    }
    return token;
  }


  //
  // Register account path
  //
  // Don't even register account system for read-only API
  if(!this.options.readOnly){
    router.use("/accounts", getAccountRouter.bind(this)());
  }

  //
  // Register paths for content
  //
  router.use("/content", getContentRouter.bind(this)());

  //
  // Error handling
  //
  // Exceptions
  router.use(endpointErrorHandler = (err, req, res, next)=>{
    // 405
    if(
      !("_all" in req.route.methods) &&
      !req.route.methods[req.method.toLowerCase()]
    ) {
      return res.respond(
        405,
        "Method not allowed",
        "This endpoint or resource doesn't support this method"
      );
    }
    // 404
    else if(!err || err.status == 404)       return next();
    // Already handled error
    else if(err.name == "HandledError") return;
    // SQLite error
    else if(err.name == "SqliteError")  err = this.parseSqliteError(err);

    return res.sendException(err);
  });
  // 404
  router.use(fallback404 = (_,res)=>{
    return res.respond(
      404,
      "Not found",
      "Given URL does not correspond to any available resource"
    );
  });

  return router;
};
