"use strict";

function factory(name="UnnamedError", status=500) {
  const e = class extends Error { };
  Object.assign(e.prototype, {
    name: name+"Error",
    status: status
  });
  return e;
}

module.exports = {
  HandledError: factory("Handled"),
  e404: factory("NotFound", 404),
  e405: factory("MethodNotAllowed", 405),
};
