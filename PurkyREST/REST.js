const e = require("./REST_errors.js");

module.exports = {
  /**
   * getRequiredParams - Checks parameters
   *
   * @param  {type} params
   * @return {type}  description
   */
  getRequiredParams: function getRequiredParams(table={}, from="*") {
    const missing = [];
    const invalid = [];
    const params = {};
    if(from=="*" || from.includes("cookie")) Object.assign(params, this.req.cookies);
    if(from=="*" || from.includes("body"))   Object.assign(params, this.req.body);
    if(from=="*" || from.includes("query"))  Object.assign(params, this.req.query);

    // Loop
    for(const key in table){
      const reqType = table[key];
      if(
        !(key in params) ||
        params[key] == null ||
        params[key] == ""
      ) {
        missing.push(key);
      }
      else if(
        reqType != "any" &&
        typeof params[key] != reqType
      ) {
        invalid.push(key);
      }
    }

    // Check missing
    if(missing.length > 0) {
      this.sendMissingFields(missing);
      throw new e.HandledError();
    }
    // Check invalid
    if(invalid.length > 0) {
      this.sendInvalidFields(invalid);
      throw new e.HandledError();
    }

    return params;
  },

  /**
   * sendAuthFailure - Sends auth failure as given response
   *
   * @param  {type} res description
   * @return {type}     description
   */
  sendAuthFailure: function sendAuthFailure(isLogin=false) {
    return this.respond(
      401,
      "Authentification failure",
      isLogin ?
        "Incorrect credentials" :
        "Provided token is expired or invalid"
    );
  },

  /**
   * sendMissingFields - Send missing fields error
   *
   * @param  {type} res       description
   * @param  {type} fields=[] description
   * @return {type}           description
   */
  sendMissingFields: function sendMissingFields(fields=[]) {
    return this.respond(
      400,
      "Not enough data",
      `The field(s) ${fields.map(a=>`\`${a}\``).join(",")} is/are required.`
    );
  },

  /**
   * sendMissingFields - Send missing fields error
   *
   * @param  {type} res       description
   * @param  {type} fields=[] description
   * @return {type}           description
   */
  sendInvalidFields: function sendInvalidFields(fields=[]) {
    return this.respond(
      400,
      "Invalid data",
      `The field(s) ${fields.map(a=>`\`${a}\``).join(",")} is/are invalid.`
    );
  },

  /**
   * sendException - Sends error/exception
   *
   * @param  {type} res description
   * @param  {type} e   description
   * @return {type}     description
   */
  sendException: function sendException(e={}) {
    return this.respond(
      e.status  || 500,
      e.name    || "UnnamedException",
      e.body    || String(e)
    );
  },

  /**
   * sendSuccess - Sends success
   *
   * @param  {type} res description
   * @return {type}     description
   */
  sendSuccess: function sendSuccess(data={}, code=200) {
    return this.respond(
      code,
      "Success",
      data
    );
  },

  /**
   * sendResourceUnspecified - Sends error that resource was not specified
   *
   * @param  {type} res description
   * @return {type}     description
   */
  sendResourceUnspecified: function sendResourceUnspecified() {
    return this.respond(
      400,
      "Resource not specified",
      "Given URI doesn't specify any particular resource, maybe you're trying to access an endpoint itself."
    );
  },

  /**
   * sendResourceNotFound - Sends error that resource was not specified
   *
   * @param  {type} res description
   * @return {type}     description
   */
  sendResourceNotFound: function sendResourceNotFound() {
    return this.respond(
      404,
      "Resource not found",
      "The resource with given URN doesn't exist"
    );
  },

  /**
   * respond - Generates API consistent response
   *
   * @param  {type} res  description
   * @param  {type} code description
   * @param  {type} name description
   * @param  {type} data description
   * @return {type}      description
   */
  respond: function respond(code=200, name="", data={}) {
    let obj = {};

    // is success?
    if(code.toString()[0] == 2){
      obj = {
        success: true,
        info: name,
        result: data,
        code: code
      };
    } else {
      obj = {
        failed: true,
        error: name,
        description: data,
        code: code
      };
    }

    return this.status(code).send(obj);
  }
};
