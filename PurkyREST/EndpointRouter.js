const ResourceRouter = require("./ResourceRouter.js");

module.exports = class EndpointRouter extends ResourceRouter{
  constructor(...args){
    super(...args);

    //
    // Register special paths
    //
    this.all("/", (_, res)=>{
      return res.sendResourceUnspecified();
    });
  }
}
