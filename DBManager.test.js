const DBManager = require('./DBManager');
const fs = require('fs');

const tmpdb = "/tmp/test-"+(Math.floor(Math.random()*1000))+".db";
const testType = {
  name: "test",
  fields: {
    content: "text"
  }
};


const end = (a,c=0)=>{console.log("\n***\n\n", a, "\n"); process.exit(c)}
const fail = a=>end(a,1);
const log = console.log;

try{
  log("Creating DB...");
  const db = new DBManager(tmpdb, a=>console.log("# SQL:", a));

  log("Registering content type...");
  db.addType(testType);

  log("Checking content types...");
  let types = db.getTypes();
  console.log(types);
  if(!types.includes("test")) fail("Type wasn't registered");

  log("Creating user...");
  db.createAccount("test", "test", "*");

  log("Creating second user...");
  db.createAccount("test2", "test2", "");

  log("Logging in...");
  const user = db.login("test", "test");
  if(!user) fail("Couldn't login");

  log("Inserting data...");
  for(let i = 1; i <= 10; i++){
    db.insertData("test", user.id, ["Hello world! Try #"+i+"."]);
  }

  log("Logging in with second user...");
  const user2 = db.login("test2", "test2");
  if(!user2) fail("Couldn't login");

  log("Trying not to insert data...");
  let insertfailed = false;
  try{
    db.insertData("test", user2.id, ["Not Hello world!"]);
  } catch (e){
    console.warn(e.name);
    if(e.name == "NotPermittedError") insertfailed = true;
    else throw e;
  }
  if(!insertfailed) fail("Insert shouldn't success, but did");

  log("Removing second account...");
  db.deleteUser(user2.id);
  const users = db.getUsers();
  console.log(users);
  if(users.length > 1) fail("Got too many users!");

  log("Retrieving data...");
  const resultData = db.getData("test", 4, 4, "id");
  console.log(resultData);
  if(resultData.length > 4) fail("Got too many data!");
  if(resultData[0].id != 5) fail("Invalid offset!");

  log("Deleting data...");
  for(let i = 1; i <= 5; i++){
    db.deleteData("test", i);
  }

  log("Retrieving data...");
  const resultData2 = db.getData("test");
  if(resultData2.length > 5) fail("Got too many data!");


  end("Test passed");
}
finally{
  fs.unlinkSync(tmpdb);
}
