const Database = require('better-sqlite3');
const YAML = require('yaml');
const fs = require('fs');
require("../latinise.js"); // Modifies String.prototype

const maxCacheSize = 536_870_912; // in bytes

class DBManager{
  constructor(dbfile, logger=()=>{}, readOnly=false, options={}){
    this.logger = logger;
    this.options = Object.assign({
      sanitization: {
        enabled: true,
        allowedTags: [],
      }
    }, options);

    this.commands = {
    };

    this.types = {};

    //
    // Start DB connection
    //
    this.db = new Database(dbfile, {
      verbose : a=>this.logger(a),
      readonly: readOnly,
    });
    this.close = ()=>this.db.close();

    // Set DB options
    this.db.pragma('journal_mode = WAL');

    // Add custom functions
    this.db.function('slugify', this.slugify);

    // Prevent DB cache from overgrowing
    setInterval(fs.stat.bind(null, dbfile+"-wal", (err, stat)=>{
      if (err) {
        if (err.code !== 'ENOENT') throw err;
      } else if (stat.size > maxCacheSize) {
        this.db.pragma('wal_checkpoint(RESTART)');
      }
    }), 5000).unref();

    // Init
    this.acc_placeholder = true;
    this.initAccounts();
  }

  /**
   * slugify - Create an URL safe string from string
   *
   * @param  {type} str description
   * @return {type}     description
   */
  slugify(str) {
    return ((str || "")
      .trim()
      .latinise()
      .replace(/((?!\w).)+/g, "-")
      .replace(/^-/, "")
      .replace(/-$/, "")
      .toLowerCase()
    );
  }

  /**
   * parseSqliteError - Parses sqlite error for machine to use
   *
   * @param  {type} err description
   * @return {type}     description
   */
  parseSqliteError(err){
    const errors = {
      failedConstraint: {
        re: /^(?<subject>[\w ]+) constraint failed: (?<table>\S+)\.(?<field>\w+)$/,
        info: { status: 409, name: "AlreadyExists" }
      },
    };

    // find the error
    for(const type in errors){
      const error = errors[type];
      const result = error.re.exec(err.message);
      if(!result) continue;

      const body = Object.assign({}, result.groups);
      if(result.groups.field in this.LANG){
        body.field_localized = this.LANG[result.groups.field][0];
      }

      Object.assign(err, {}, error.info, {
        type: type,
        body: body,
      });
      break;
    }

    return err;
  }
}

//
// STATIC CONSTANTS
//
DBManager.prototype.CONTENT_TABLE_PRECEPT = {
  table: [
    "id INTEGER PRIMARY KEY AUTOINCREMENT",
    "ctime TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL",
    "mtime TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL",
    "ftsID INTEGER",
    "slug TEXT UNIQUE", // unique URN
    "categorySlug TEXT",
    "authorID INTEGER NOT NULL",
    "isPublic BOOLEAN DEFAULT 1",
    "category TEXT",
    "title TEXT UNIQUE NOT NULL",
  ],
  systemFields:     ["id", "ctime", "mtime", "ftsID", "slug", "categorySlug"],
  builtinFields:    ["authorID", "isPublic", "category", "title"],
  searchableFields: ["title"],
};
DBManager.prototype.ACC_TABLE = "accounts";
DBManager.prototype.CONT_TABLE_PREF = "content_";
DBManager.prototype.SLUG_FROM = "title";
DBManager.prototype.FTS_TABLE_SUFF = "_fts";

DBManager.prototype.CONTENT_TABLE_PRECEPT.reservedKeys = [
  ...DBManager.prototype.CONTENT_TABLE_PRECEPT.systemFields,
  ...DBManager.prototype.CONTENT_TABLE_PRECEPT.builtinFields
];

DBManager.prototype.LANG = YAML.parse(String(fs.readFileSync(__dirname+"/../lang.yml")));

//
// IMPORT METHODS
//
Object.assign(DBManager.prototype, require("./_Accounts.js"));
Object.assign(DBManager.prototype, require("./_ContentTypes.js"));


//
// EXPORT
//
module.exports = DBManager;


//
// HELPER FUNCTIONS
//
/*function compileTransaction(db, sqlArray) {
  const statements = sqlArray.map(sql => db.prepare(sql));
  return db.transaction((data = {}) => {
    let result;
    statements.forEach((stmt, i) => {
      if (stmt.reader) result = stmt.get(data);
      else stmt.run(data);
    });
    return result;
  });
}*/
