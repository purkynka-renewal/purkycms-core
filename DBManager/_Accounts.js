const bcrypt = require("bcrypt");

module.exports = {

  /**
   * initAccounts - Inits account DB
   *
   * @return {undefined}
   */
  initAccounts: function initAccounts() {
    this.db.prepare(
      `CREATE TABLE IF NOT EXISTS '${this.ACC_TABLE}' (`+
        `id INTEGER PRIMARY KEY UNIQUE, `+
        `login TEXT NOT NULL UNIQUE, `+
        `passwd BLOB, `+
        `perms TEXT, `+
        `createdBy INTEGER`+
      `)`
    ).run();

    this.commands.acc = {
      delete: this.db.prepare(
        `DELETE FROM '${this.ACC_TABLE}' WHERE id = ?`
      ),
      select: this.db.prepare(
        `SELECT id,login,perms,createdBy `+
        `FROM '${this.ACC_TABLE}' `+
        `ORDER BY ? LIMIT ? OFFSET ?`
      ),
      create: this.db.prepare(
        `INSERT INTO '${this.ACC_TABLE}' (login, passwd, perms, createdBy) `+
        `VALUES (@login, @passwd, @perms, @createdBy)`
      ),
      login: this.db.prepare(
        `SELECT id,passwd FROM '${this.ACC_TABLE}' WHERE login = ?`
      ),
      getUser: this.db.prepare(
        `SELECT id,login,perms FROM '${this.ACC_TABLE}' WHERE id = ?`
      )
    };

    // Create placeholder account if there is none
    const nacc = this.db.prepare(`SELECT COUNT() FROM ${this.ACC_TABLE}`).get();
    if(nacc["COUNT()"] === 0){
      this.commands.acc.create.run({
        login:  "root",
        passwd: getHash("ef5b84c0e94bec33"),
        perms:  "[\"*\"]",
        createdBy: 0
      });
      this.acc_placeholder = true;
    }
  },

  /**
   * createAccount - Creates an account, deletes placeholder account if present
   *
   * @param  {String} login  description
   * @param  {String} passwd description
   * @param  {Array}  roles  description
   * @return {undefined}        description
   */
  createAccount: function createAccount(login, passwd, perms, authorID){
    if(this.acc_placeholder){
      this.commands.acc.delete.run(1);
      this.acc_placeholder = false;
    }

    if(!Array.isArray(perms)) perms = [perms];

    const result = this.commands.acc.create.run({
      login:  login,
      passwd: getHash(passwd),
      perms:  JSON.stringify(perms),
      createdBy: authorID
    });

    return {
      id: result.lastInsertRowid,
      login: login,
      perms: perms
    };
  },

  /**
   * login - Try to login given login name with given password
   *
   * @param  {type} login  description
   * @param  {type} passwd description
   * @return {type}        description
   */
  login: function login(login, passwd) {
    const result = this.commands.acc.login.get(login);
    if(result && bcrypt.compareSync(passwd, result.passwd)){
      return this.commands.acc.getUser.get(result.id);
    }
    return false;
  },

  /**
   * getUser - Gets user data
   *
   * @param  {Number} id description
   * @return {Object}    description
   */
  getUser: function getUser(id) {
    const user = this.commands.acc.getUser.get(id);
    user.perms = JSON.parse(user.perms);
    return user;
  },

  /**
   * getUsers - gets users
   *
   * @param  {type} from=0       description
   * @param  {type} limit=8      description
   * @param  {type} orderby="id" description
   * @return {type}              description
   */
  getUsers: function getUsers(from=0, limit=8, orderby="id") {
    return this.commands.acc.select.all(
      orderby,
      limit,
      from
    );
  },

  /**
   * deleteUser - deletes user
   *
   * @param  {type} id description
   * @return {type}    description
   */
  deleteUser: function deleteUser(id) {
    return this.commands.acc.delete.run(id);
  },

  /**
   * hasPermsTo - Checks if user can edit given content type
   *
   * @param  {Number} id      description
   * @param  {String} content description
   * @return {Boolean}         description
   */
  hasPermsTo: function hasPermsTo(id, content) {
    const user = this.getUser(id);

    return user.perms.includes("*") || user.perms.includes(content);
  },
};


function getHash(str) {
  return bcrypt.hashSync(str, 10);
  // BUG: async doesn't work... node v14 problem?
}
