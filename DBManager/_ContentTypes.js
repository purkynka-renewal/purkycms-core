const errors = require('../errors.js');
const insane = require('insane');

module.exports = {
  addType: require("./_ContentTypes_addType.js"), // imported

  getTypes: function getTypes() {
    return Object.keys(this.types);
  },

  getType: function getType(type) {
    return this.types[type];
  },

  /**
   * _insertCheckFields - Checks validity of provided data
   *
   * @param  {type} fields  description
   * @param  {type} allowed description
   * @return {type}         description
   */
  _insertCheckFields: function _insertCheckFields(fields, allowed) {
    fields.forEach((field)=>{
      if(!allowed.includes(field)) throw new errors.InvalidField(
        `Field '${field}' is not a valid field of this content type`
      );
      else if(field in this.CONTENT_TABLE_PRECEPT.systemFields) {
        throw new errors.InvalidField(
          `Field '${field}' is a system field, unwritable by API`
        );
      }
    });
  },

  _sanitizeData: function _sanitizeData(data){
    for(const key in data){
      let item = data[key];
      switch (typeof item) {
        case "boolean":
          item = Number(item);
          break;

        case "object":
          item = JSON.stringify(item);

        case "string":
          if (this.options.sanitization.enabled) {
            item = insane(item, this.options.sanitization);
          }
      }
      data[key] = item;
    }
    return data;
  },

  /**
   * insertData - Inserts new row to the table of given type
   *
   * @param  {String} type  description
   * @param  {Array} data   description
   * @return {Bool}         description
   */
  insertData: function insertData(type, authorID, data) {
    if(!(type in this.types)) throw new ReferenceError(
      `Content type '${type}' is not registered`
    );
    if(!this.hasPermsTo(authorID, type)) throw new errors.NotPermitted();

    const fields = Object.keys(data);
    const allowed = [
      ...this.CONTENT_TABLE_PRECEPT.builtinFields,
      ...Object.keys(this.types[type].fields)
    ];

    this._insertCheckFields(fields, allowed);
    data = this._sanitizeData(data);

    return this.db.prepare(
      `INSERT INTO '${this.CONT_TABLE_PREF+type}' `+
        `(authorID,${ fields.join(",") }) `+
      `VALUES `+
        `(@authorID, ${ fields.map(a=>"@"+a).join(", ") })`
    ).run({authorID: authorID, ...data});
  },


  /**
   * updateData - Updates existing row of the table of given type
   *
   * @param  {type} type     description
   * @param  {type} slug     description
   * @param  {type} authorID description
   * @param  {type} data     description
   * @return {type}          description
   */
  updateData: function updateData(type, slug, authorID, data) {
    if(!(type in this.types)) throw new ReferenceError(
      `Content type '${type}' is not registered`
    );
    if(!this.hasPermsTo(authorID, type)) throw new errors.NotPermitted();

    const fields = Object.keys(data);
    const allowed = [
      ...this.CONTENT_TABLE_PRECEPT.builtinFields,
      ...Object.keys(this.types[type].fields)
    ];

    this._insertCheckFields(fields, allowed);
    data = this._sanitizeData(data);

    return this.db.prepare(
      `UPDATE '${this.CONT_TABLE_PREF+type}' SET `+
        fields.map(a=>a+" = "+"@"+a).join(", ") +" "+
        `WHERE slug = @slug`
    ).run({...data, slug: slug});
  },

  /**
   * getData - Gets data of given type
   *
   * @return {type}         description
   */
  getCount: function getCount(type) {
    return this.commands[this.CONT_TABLE_PREF+type].getCount.get();
  },

  /**
   * getData - Gets data of given type
   *
   * @param  {type} type    description
   * @param  {type} from=0  description
   * @param  {type} limit=8 description
   * @return {type}         description
   */
  getData: function getData(type, from=0, limit=8, orderby="id") {
    return (this.commands[this.CONT_TABLE_PREF+type].select.all(
      orderby,
      limit,
      from
    ) || []).map(parseOutput);
  },

  /**
   * getDataBySlug - gets data by slug
   *
   * @return {type}  description
   */
  getDataBySlug: function getDataBySlug(type, slug) {
    return parseOutput(this.commands[this.CONT_TABLE_PREF+type].selectSlug.get(slug));
  },


  /**
   * getDataByCategory - description
   *
   * @param  {type} type         description
   * @param  {type} category     description
   * @param  {type} from=0       description
   * @param  {type} limit=8      description
   * @param  {type} orderby="id" description
   * @return {type}              description
   */
  getDataByCategory: function getDataByCategory(
    type,
    category,
    from=0,
    limit=8,
    orderby="rank"
  ) {
    return (this.commands[this.CONT_TABLE_PREF+type].selectCategory.all(
      category,
      orderby,
      limit,
      from
    ) || []).map(parseOutput);
  },

  /**
   * getDataByFTS - description
   *
   * @param  {type} type         description
   * @param  {type} query        description
   * @param  {type} from=0       description
   * @param  {type} limit=8      description
   * @param  {type} orderby="id" description
   * @return {type}              description
   */
  getDataByFTS: function getDataByFTS(
    type,
    query,
    from=0,
    limit=8,
    orderby="id"
  ) {
    return (this.commands[this.CONT_TABLE_PREF+type].selectFTS.all(
      query,
      orderby,
      limit,
      from
    ) || []).map(parseOutput);
  },

  /**
   * getCategories - Fetches all used catgories from the table
   *
   * @param  {type} type description
   * @return {type}      description
   */
  getCategories: function getCategories(type) {
    return this.commands[this.CONT_TABLE_PREF+type].getCategories
        .all().map(a=>a.category).filter(v=>v);
  },

  /**
   * deleteData - Deletes data
   *
   * @return {type}  description
   */
  deleteData: function deleteData(type, id) {
    return this.commands[this.CONT_TABLE_PREF+type].delete.run(id);
  }
}


function parseOutput(row) {
  for(const key in row){
    row[key] = object2JSON(row[key]);
  }
  return row;
}

function object2JSON(obj) {
  const str = String(obj);
  if(
    (str.startsWith("{") && str.endsWith("}")) ||
    (str.startsWith("[") && str.endsWith("]"))
  ){
    try{
      return JSON.parse(str);
    }catch(e){}
  }
  return obj;
}
