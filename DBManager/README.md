# Database Manager
SQLite interface


## Constructor parameters
1. `dbfile` &ndash; path to .db file which will be used or created
2. `logger` &ndash; function to call with every SQL query


## Methods
### `.addType(<type>)` _-&gt; String or Bool_
(Re)registers content type.

Returns `true` if ok, `false` if the type name is already registered or one of the following warnings:
+ `outdated` if the provided type doesn't correspond to the DB

#### Parameters
  + Object `<type>` &ndash; Prescription of content type as defined in `../types/README.md`


### `.getTypes()` _-&gt; Array of Strings_
Returns list of content type names currently registered

### `.getType(<type>)` _-&gt; Object_
Returns prescription Object of given type

#### Parameters
  + String `<type>` &ndash; Name of the type to return
