const STORAGETYPES = require("./storage-types.json");

module.exports = function addType(type){
  let status = "ok";
  const name = type.name.toLowerCase();
  if(name in this.types) return false;
  const tableName = `${this.CONT_TABLE_PREF}${name}`;
  const tableNameFTS = `${tableName}${this.FTS_TABLE_SUFF}`;
  const insertableFields = [
    ...Object.keys(type.fields),
    ...this.CONTENT_TABLE_PRECEPT.builtinFields
  ];

  if(this.options.readOnly){
    this.types[name] = {...type, table:{status:"readOnly"}};
    prepareCommands.bind(this)(tableName, tableNameFTS);
    return "readOnly";
  }

  //// Generate table definiton
  const table = [...this.CONTENT_TABLE_PRECEPT.table];
  //// Generate FTS table definiton
  const tableFTS = [...this.CONTENT_TABLE_PRECEPT.searchableFields];

  for(const key in type.fields){
    const field = type.fields[key];
    const fieldType = (field.type || "text").split(".");

    if (this.CONTENT_TABLE_PRECEPT.reservedKeys.includes(key)) {
      throw new errors.InvalidContentType(
        `'${key}' in '${name}' content type is a reserved field key`
      );
    }

    let datatype;
    for (const storagetype in STORAGETYPES){
      if(STORAGETYPES[storagetype].some(a=>a==fieldType[fieldType.length-1])){
        datatype = storagetype;
        break;
      }
    }
    if(!datatype) {
      throw new errors.InvalidContentType(
        `Invalid type '${fieldType.join(".")}' with key '${key}' `+
        `in '${name}' content type`
      );
    }

    table.push(`${key} ${datatype}`);
    if(field.searchable) tableFTS.push(key);
  }

  //
  // CREATE TABLE
  //
  //// Make sure the table is created
  this.db.prepare(
    `CREATE TABLE IF NOT EXISTS '${tableName}'`+
    `(${table.map(a=>`'${a}'`).join(", ")})`
  ).run();

  //// Make sure the FTS table is created if needed
  if(tableFTS.length > 0){
    this.db.prepare(
      `CREATE VIRTUAL TABLE IF NOT EXISTS '${tableNameFTS}' `+
      `USING fts5(${tableFTS.map(a=>`'${a}'`).join(", ")})`
    ).run();
  }

  //
  // CREATE TRIGGERS
  //
  createTriggers.bind(this)(type, tableName, tableNameFTS, tableFTS);

  //
  // Prepare SQL commands
  //
  prepareCommands.bind(this)(tableName, tableNameFTS);

  //// Check if the table is correct and up-to-date
  // Get table description
  const tableSQL = this.db.prepare(
    `SELECT sql FROM sqlite_master WHERE name = '${tableName}'`
  ).get().sql.replace(/.*\((.*)\)/, "$1").split(/,(?: )?/);

  // Compare the tables
  if(tableSQL.join(",") != table.join(",")) status = "outdated";
  // TODO: alter table according to delta of changes (maybe ask the admin first?)
  type.table = {
    sql: table,
    status: status,
    insertableFields: insertableFields,
    hasFTS: tableFTS.length > 0
  };
  this.types[name] = type;

  return status;
};


/**
 * prepareCommands - Prepares SQL commands for given table
 * Bind this function to DBManager object, please
 *
 * @param  {type} name             description
 * @param  {type} insertableFields description
 * @param  {type} tableName        description
 * @param  {type} tableNameFTS     description
 * @return {type}                  description
 */
function prepareCommands(tableName, tableNameFTS) {
  this.commands[tableName] = {
    //// Insert data to the table
    // insert: this.db.prepare(
    //   `INSERT INTO '${tableName}' `+
    //     `(${ insertableFields.join(",") }) `+
    //   `VALUES `+
    //     `(${ insertableFields.map(a=>(`@${a}`)).join(",") })`
    // ),
    // // Select amount of data from the table
    select: this.db.prepare(
      `SELECT `+
        `author.login AS author, `+
        `content.* `+
      `FROM '${tableName}' AS 'content' `+
      `LEFT JOIN ${this.ACC_TABLE} AS 'author' ON `+
        `author.id = content.authorID `+
      `ORDER BY ? LIMIT ? OFFSET ?`
    ),
    //// Select data by ID from the table
    selectID: this.db.prepare(
      `SELECT `+
        `author.login AS author, `+
        `content.* `+
      `FROM '${tableName}' AS 'content' `+
      `LEFT JOIN ${this.ACC_TABLE} AS 'author' ON `+
        `author.id = content.authorID `+
      `WHERE content.id = ? `
    ),
    //// Select data by slug from the table
    selectSlug: this.db.prepare(
      `SELECT `+
        `author.login AS author, `+
        `content.* `+
      `FROM '${tableName}' AS 'content' `+
      `LEFT JOIN ${this.ACC_TABLE} AS 'author' ON `+
        `author.id = content.authorID `+
      `WHERE slug = ? `
    ),
    //// Select data by category from the table
    selectCategory: this.db.prepare(
      `SELECT `+
        `author.login AS author, `+
        `content.* `+
      `FROM '${tableName}' AS 'content' `+
      `LEFT JOIN ${this.ACC_TABLE} AS 'author' ON `+
        `author.id = content.authorID `+
      `WHERE categorySlug = ? `+
      `ORDER BY ? LIMIT ? OFFSET ? `
    ),
    //// Select data using FTS
    selectFTS: this.db.prepare(
      `SELECT `+
        `author.login AS author, `+
        `content.* `+
      `FROM '${tableName}' AS 'content' `+
      `LEFT JOIN ${this.ACC_TABLE} AS 'author' ON `+
        `author.id = content.authorID `+
      `WHERE ftsID = (SELECT rowid FROM ${tableNameFTS}(?)) `+
      `ORDER BY ? LIMIT ? OFFSET ? `
    ),
    //// Gets all unique categories
    getCategories: this.db.prepare(
      `SELECT DISTINCT category `+
      `FROM '${tableName}' `+
      `ORDER BY category`
    ),
    //// Delete data by ID from the table
    delete: this.db.prepare(
      `DELETE FROM '${tableName}' WHERE id = ?`
    ),
    getCount: this.db.prepare(
      `SELECT COUNT() as count `+
      `FROM '${tableName}' `
    ),
  };
}


 /**
  * createTriggers - Creates standard triggers for table if needed
  * Bind this function to DBManager object, please
  *
  * @param  {type} type         description
  * @param  {type} tableName    description
  * @param  {type} tableNameFTS description
  * @param  {type} tableFTS     description
  * @return {type}              description
  */

function createTriggers(type, tableName, tableNameFTS, tableFTS) {
  //// Make sure time update trigger is present
  this.db.prepare(
    `CREATE TRIGGER IF NOT EXISTS 'afterUpdate_${tableName}' `+
    `AFTER UPDATE `+
    `ON '${tableName}' `+
    `FOR EACH ROW `+
    `WHEN NEW.mtime <= OLD.mtime `+ // Avoids "while true"
    `BEGIN `+
      /// Update mtime and generate slug when we're at it, why not
      `UPDATE '${tableName}' SET `+
        `mtime=CURRENT_TIMESTAMP, `+
        `slug=slugify(NEW.${this.SLUG_FROM}), `+
        `categorySlug=slugify(NEW.category) `+
      `WHERE id = NEW.id; `+
      // TODO: Log changes to ring-buffer log table
    `END`
  ).run();

  //// Generate slug on insert
  this.db.prepare(
    `CREATE TRIGGER IF NOT EXISTS 'afterInsert_${tableName}' `+
    `AFTER INSERT `+
    `ON '${tableName}' `+
    `FOR EACH ROW `+
    `BEGIN `+
      /// Generate slug when we're at it, why not
      `UPDATE '${tableName}' SET `+
        `slug=slugify(NEW.${this.SLUG_FROM}), `+
        `categorySlug=slugify(NEW.category) `+
      `WHERE id = NEW.id; `+
      // TODO: Log changes to ring-buffer log table
    `END`
  ).run();

  //
  //// Sync with FTS table
  if(tableFTS.length > 0){
    /// INSERT
    this.db.prepare(
      `CREATE TRIGGER IF NOT EXISTS 'ftssync_insert_${tableName}' `+
      `AFTER INSERT `+
      `ON '${tableName}' `+
      `FOR EACH ROW `+
      `BEGIN `+
        // Copy the row into FTS table
        `INSERT INTO '${tableNameFTS}' `+
          `(${ tableFTS.join(",") }) `+
        `VALUES `+
          `(${ tableFTS.map(a=>("NEW."+a)).join(",") }); `+
        // Save rowid in FTS table into normal table
        `UPDATE '${tableName}' `+
        `SET ftsID = last_insert_rowid() `+
        `WHERE id = NEW.id; `+
      `END`
    ).run();

    /// UPDATE
    this.db.prepare(
      `CREATE TRIGGER IF NOT EXISTS 'ftssync_insert_${tableName}' `+
      `AFTER UPDATE `+
      `ON '${tableName}' `+
      `FOR EACH ROW `+
      `WHEN ${
        tableFTS.map(a=>(`NEW.${a} <> OLD.${a}`)).join(" OR ")
      } `+ // Apply only when applicable fields change
      `BEGIN `+
        `UPDATE '${tableNameFTS}' SET ${
          tableFTS.map(a=>(`${a}=NEW.${a}`)).join(", ")
        } `+
        `WHERE rowid = NEW.id;` +
      `END`
    ).run();

    /// DELETE
    this.db.prepare(
      `CREATE TRIGGER IF NOT EXISTS 'ftssync_insert_${tableName}' `+
      `AFTER DELETE `+
      `ON '${tableName}' `+
      `FOR EACH ROW `+
      `BEGIN `+
        `DELETE FROM '${tableNameFTS}' WHERE rowid = OLD.id; `+
      `END`
    ).run();
  }
}
