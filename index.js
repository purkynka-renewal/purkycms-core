const express       = require('express');
const runMiddleware = require('run-middleware');
const TokenManager  = require("./TokenManager");
const TYPES         = require('./types');
const DBManager     = require('./DBManager');
const PurkyREST     = require("./PurkyREST");

const Debug = require('debug');

const debug = Debug("purky");


module.exports = class Purky extends DBManager{
  constructor(inOptions){
    debug("Initializing...");
    const options = Object.assign({

      /**
       * @option dbpath Path to DB file to create and/or connect to
       */
      dbpath: "./database.db",

      /**
       * @option pageLength Max. number of items in listing on one page
       */
      pageLength: 16,

      /**
       * @option readOnly
       */
      readOnly: false,
      sanitization: {
        enabled: true,
        allowedTags: []
      }
    }, inOptions || {});
    super(
      options.dbpath,
      a=>Debug("sqlite")("SQL command: %s", a),
      options.readOnly,
      options,
    );
    this.options      = options;
    this.tokenManager = new TokenManager();
    this.router       = (_,_1,next)=>next();

    this.dbApp = express();
    this.dbApp.disable("x-powered-by");
    const replacableDBRouter = (req,res,next)=>{
      return this.router(req,res,next);
    };
    this.dbApp.use(replacableDBRouter);
    runMiddleware(this.dbApp);
    debug("Initialized");
  }

  /**
   * load - Loads content types and prepares data resolver
   *
   * @return {type}  description
   */
  load(types=TYPES) {
    debug("Loading started...");
    for(const type in types){
      debug(`Loading content type '${type}'...`);
      this.addType(types[type]);
      debug(`Content type '${type}' is ready.`);
    }

    debug("Initializing REST API router...");
    this.router = PurkyREST.bind(this)();

    debug("Loading done.");
  }

  /**
   * checkToken - Checks token validity
   *
   * @param  {type} token="" description
   * @return {type}          description
   */
  checkToken(token="") {
    return this.tokenManager.get(token);
  }

  /**
   * getToken - description
   *
   * @return {type}  description
   */
  getToken(data){
    return this.tokenManager.newToken({
      clientId: data.id,
      // expiration: 1000 * 60 * 30, // 30 minutes
      data
    })
  }

  /**
   * resolveURI - Resolves data URI
   *
   * @param  {String} uri          description
   * @param  {Object} query={}
   * @param  {Object} data={}      description
   * @param  {String} method="get" description
   * @param  {String} token        description
   * @return {type}              description
   */
  resolveURI(uri, query={}, body={}, method="get") {
    debug("Recieved internal API request");
    return new Promise((resolve)=> {
      this.dbApp.runMiddleware(
        uri,
        {
          method: method,
          body: body,
          query: {
            ...query
          }
        },
        (c,d,h)=>resolve({
          code    :c,
          data    :d,
          headers :h,
        })
      );
    });
  }
}

module.exports.getRandomString = module.exports.prototype.getRandomString = TokenManager.Token.getRandomString;
