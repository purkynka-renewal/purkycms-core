const fs = require('fs');
const Purky = require("./");

const end = (a,c=0)=>{console.log("\n***\n\n", a, "\n"); process.exit(c)}
const fail = a=>end(a,1);
const log = console.log;

const tmpdb = "/tmp/test-"+(Math.floor(Math.random()*1000))+".db";


(async ()=>{
  log("Initializing Purky...");
  const purky = new Purky({
    dbpath: tmpdb,
    salt: "secret"
  });

  log("Purky is loading...");
  purky.load();

  log("Testing account router...");
  const result0 = await purky.resolveURI("/accounts/_ping");
  log("> ping :", result0.data);
  if(result0.code != 200) fail("Router ping failed");

  log("Testing data router...");
  const result1 = await purky.resolveURI("/content/_ping");
  log("> ping :", result1.data);
  if(result1.code != 200) fail("Router ping failed");

  log("Trying invalid method...");
  const result1_2 = await purky.resolveURI("/content/_ping", {}, {}, "POST");
  log(">", result1_2.data);
  if(result1_2.code == 200) fail("Invalid method succeeded but shouldn't");

  log("Trying to fail POST data without token...");
  const result2 = await purky.resolveURI("/content/articles", {
  }, {title: "hi", content: "Hi"}, "POST");
  log(">", result2.data);
  if(result2.code == 200) fail("operation succeeded, but shouldn't");

  log("Trying to fail POST data with invalid token...");
  const result2_2 = await purky.resolveURI("/content/articles", {
    token: "adhiahiuwgfdiugfw8"
  }, {content: "Hi"}, "POST");
  log(">", result2_2.data);
  if(result2_2.code == 200) fail("operation succeeded, but shouldn't");

  log("Logging in...");
  const result3 = await purky.resolveURI("/accounts/login", {}, {
    username: "root",
    password: "ef5b84c0e94bec33"
  }, "POST");
  log(">", result3.data);
  if(result3.code != 200) fail("login failed");

  log("Creating new account...");
  const result4 = await purky.resolveURI("/accounts/create", {
    token: result3.data.result.token
  }, {
    user: "testuser",
    password: "testuser",
    params: ["*"]
  }, "POST");
  log(">", result4.data);
  if(result4.code != 200) fail("Creating failed");

  log("Logging out...");
  const result5 = await purky.resolveURI("/accounts/logout", {
    token: result3.data.result.token
  });
  log(">", result5.data);
  if(result5.code != 200) fail("logout failed");

  log("Trying to fail POST data with expired token...");
  const result6 = await purky.resolveURI("/content/articles", {
    token: result3.data.result.token
  }, {content: "Hi"}, "POST");
  log(">", result6.data);
  if(result6.code == 201) fail("operation succeeded, but shouldn't");

  log("Logging in with new account...");
  const result7 = await purky.resolveURI("/accounts/login", {}, {
    username: "testuser",
    password: "testuser"
  }, "POST");
  log(">", result7.data);
  if(result7.code != 200) fail("login failed");

  log("Creating new data...");
  const result8 = await purky.resolveURI("/content/articles", {
    token: result7.data.result.token
  }, {
    title: "Test post 0",
    content: "Hi"
  }, "POST");
  log(">", result8.data);
  if(result8.code != 201) fail("Creating new data failed");

  log("Trying to fail creating the same data...");
  const result9 = await purky.resolveURI("/content/articles", {
    token: result7.data.result.token
  }, {
    title: "Test post 0",
    content: "Hi"
  }, "POST");
  log(">", result9.data);
  if(result9.code == 201) fail("Creating the same data succeeded, but shouldn't");

  log("Updating data...");
  const result10 = await purky.resolveURI("/content/articles/test-post-0", {
    token: result7.data.result.token
  }, {
    content: "Hi EDIT: once again",
    category: "testagory"
  }, "PUT");
  log(">", result10.data);
  if(result10.code != 200) fail("Updating data failed");

  log("Getting data list...");
  const result11 = await purky.resolveURI("/content/articles");
  log(">", result11.data);
  log(">", result11.data.result);
  if(result11.code != 200) fail("Retrieving index failed");

  log("Getting specific data...");
  const result12 = await purky.resolveURI("/content/articles/test-post-0");
  log(">", result12.data);
  if(result12.code != 200) fail("Retrieving data failed");

  log("Getting category list...");
  const result13 = await purky.resolveURI("/content/articles/category");
  log(">", result13.data);
  if(result13.code != 200) fail("Retrieving data failed");

  log("Getting data by category...");
  const result14 = await purky.resolveURI("/content/articles/category/testagory");
  log(">", result14.data.result);
  if(result14.code != 200) fail("Retrieving data failed");

  end("Test complete");

})()
.catch(console.error)
.finally(()=>{
  fs.unlinkSync(tmpdb);
});
