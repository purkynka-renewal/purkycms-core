# Purky

A headless CMS (Content Management System) built tailored for
[sspbrno.cz](http://sspbrno.cz/).

This system is expecting a low count of administrators and short posts for
highest possible perfomance.
- - -

## Subdocuments
+ **User**
  + [REST API](REST_API.md)
+ **Developer**
  + [DBManager](DBManager/README.md)
  + [Content Types](types/README.md)

- - -

## API

### `Purky(options)` &ndash; Main class
+ `options` Object
  + `dbpath` String &ndash; Path to database file (default: `./database.db`)
  + `pageLength` Integer &ndash; How many results to give for one page by
    default (default: `16`)
  + `readOnly` Boolean &ndash; If this session should be read-only (default:
    `false`)
    + If `true`, disables account management support (including logging in) and
    content editing
    + Useful for multi-threaded serving (admin thread should be singular)

#### `Purky.getRandomString(length)` &ndash; Generates random string
+ `length` Integer &ndash; How long the string should be (default: `8`)

#### `Purky#load(types)` &ndash; Prepares the database and APIs
+ `types` Array of Objects &ndash; Content type definitions (more in
  [Content Types](types/README.md))

#### `Purky#resolveURI(uri, query, body, method)` &ndash; Simulates API request


## Concept
This node module acts as a simplified interface between the main CMS module and
the database. That means that it defines the structure of data and limits the
types of content that the whole system can use.  

Also handles the following:

+ per-user permission, authentication and user management
+ ~~configuration and long-term site settings~~
+ REST API routing and serving

- - -

## Author

Made by Adam Žingor &lt;[adam@zhincore.eu](mailto:adam@zhincore.eu)&gt;
