const Purky = require("./");
const express = require('express');
const bodyParser = require('body-parser');


const tmpdb = "/tmp/test-"+(Math.floor(10000+Math.random()*1000))+".db";


const purky = new Purky({
  dbpath: tmpdb
});

purky.load();


const app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.use("/api", purky.router);

app.listen("8080", "127.0.0.1", ()=>{
  console.info("Listening");
});
