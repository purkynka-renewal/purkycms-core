const fs = require('fs');
const YAML = require('yaml');

const dir = fs.readdirSync(__dirname);

module.exports = {}

dir.forEach((item, i) => {
  const filename = __dirname+"/"+item;
  if(
    !(
      item.endsWith(".yml") ||
      item.endsWith(".yaml")
    ) ||
    item.startsWith("_") ||
    filename == __filename
  ) return;

  const file = fs.readFileSync(filename).toString();
  module.exports[item.replace(/\.(?:yml|yaml)$/, "")] = YAML.parse(file);
});
