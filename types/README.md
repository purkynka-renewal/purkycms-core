# Content types documentation
YML files containing prescription on how to save, load and modify given content type.  

When a content type is added, the module needs to be restarted.  
Tables of deleted/modified types need to be deleted manually.


## Structure
+ `name`: name of the content type
+ `fields`: what kind of content we will be storing:
  + &lt;key&gt;: &lt;type defined in `DBManager/storage-types.json`&gt;
