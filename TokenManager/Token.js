const crypto  = require("crypto");

module.exports = function Token(options, selfDestruct=()=>{}){
  let _dateStarted= new Date();
  let timeout;
  const _options = Object.assign({
    clientId: -1,
    tokenString: null,
    expiration: 1000 * 60 * 60 * 24, // 1 day
    data: {}
  }, options);

  if(!_options.tokenString){
    _options.tokenString = this.getRandomString(32);
  }

  /**
   * commitDeath - description
   *
   * @return {type}  description
   */
  function expire() {
    selfDestruct(_options.tokenString);
    return null;
  }

  /**
   * resetTimeout - description
   *
   * @return {type}  description
   */
  function resetTimeout() {
    if(timeout) clearTimeout(timeout);
    _dateStarted = new Date();

    timeout = setTimeout(
      expire,
      _options.expiration
    );
  }

  /**
   * isValid - description
   *
   * @return {type}  description
   */
  function isValid() {
    resetTimeout();
    return Date.now() < (_dateStarted.valueOf() + _options.expiration);
  }

  resetTimeout();

  const data = {
    tokenString : _options.tokenString,
    clientId    : _options.clientId,
    expiration  : _options.expiration,
    data        : _options.data
  };
  const self = new Object();
  self.name = "Token";
  return Object.assign(self, data, {
    get() {
      if(!isValid()) return expire();
      resetTimeout();
      return Object.assign({}, self);
    },
    expire  : expire,
    isValid : isValid,
  });
}

module.exports.getRandomString = module.exports.prototype.getRandomString = len=>crypto.randomBytes(len).toString('hex');
