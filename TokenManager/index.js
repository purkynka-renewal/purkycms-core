const Token   = require("./Token.js");

class TokenManager {
  constructor(){
    this._tokens = {};
  }

  newToken(options){
    const token = new Token(options, (...a)=>{this.expireToken(...a)});
    this._tokens[token.tokenString] = token;
    return token.get();
  }

  expireToken(tokenString){
    delete this._tokens[tokenString];
  }

  get(tokenString){
    if(!(tokenString in this._tokens)) return null;
    return this._tokens[tokenString].get();
  }
}

TokenManager.Token = Token;

module.exports = TokenManager;
