"use strict";

function factory(name="UnnamedError") {
  const e = class extends Error {};
  Object.assign(e.prototype, {
    name: name+"Error"
  });
  return e;
}

module.exports = {
  InvalidContentType: factory("InvalidContentType"),
  NotPermitted:       factory("NotPermitted"),
  InvalidField:       factory("InvalidField")
}
